package com.elao.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.elao.com.R;
import com.elao.com.utility.CommonClass;
import com.elao.com.utility.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Currency;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/**
 * <h>SplashActivity</h>
 * <p>
 *     This is launch screen i.e open first when user launch the app. It stays for
 *     3second then check if user is logged-in then go to HomeActivity or else go
 *     to Landing Screen where we have option for login or signup.
 * </p>
 * @since 3/29/2017.
 * @author 3Embed
 * @version 1.0
 */

public class SplashActivity extends AppCompatActivity
{
    private static final String TAG = SplashActivity.class.getSimpleName();
    private Activity mActivity;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        mActivity=SplashActivity.this;
        // change status bar color
        CommonClass.statusBarColor(mActivity);
         mSessionManager = new SessionManager(mActivity);
         // printHashKey();

        // get default currency
        getDefaultCurrency();

        // generating unique id from FCM
        String serialNumber = FirebaseInstanceId.getInstance().getId();
        System.out.println(TAG+" "+"serial number="+serialNumber);
        if (serialNumber!=null && !serialNumber.isEmpty())
        {
            mSessionManager.setDeviceId(serialNumber);
        }

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
        System.out.println(TAG+" "+ "My Refreshed token: " + refreshedToken);
        if (refreshedToken!=null && !refreshedToken.isEmpty())
            mSessionManager.setPushToken(refreshedToken);

        System.out.println(TAG+"get push token="+ mSessionManager.getPushToken());

        // get bundle datas if notification comes in background
        Bundle bundle =getIntent().getExtras();
        System.out.println(TAG+" "+"bundle="+bundle);

        String notificationDatas="";
        if (bundle!=null)
        {
            notificationDatas= bundle.getString("body");
            System.out.println(TAG+" "+"bundle notification datas="+notificationDatas);
        }

        // set language from sharedpref.
        String languageCode = mSessionManager.getLanguageCode();
        if (languageCode.length() == 0) {
            moveToSelectLanguageScreen();
        } else {
            // Go to notification screen if any notification msg is there else Home Page
            if (notificationDatas!=null && !notificationDatas.isEmpty())
                callNotificationClass(notificationDatas);
            else {
                setTimerForScreen();
            }
        }
        // ignore
        /*
        Log.d("getlanguage",mSessionManager.getLanguageCode());
        selectLanguage(mSessionManager.getLanguageCode());
         */
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    /**
     *
     * <h>CallNotificationClass</h>
     * <p>
     *     In this method we used to receive the bundle datas when notification comes
     *     from background. After that we used to send datas to Notification activity
     *     class.
     * </p>
     * @param notificationDatas The notification datas.
     *
     */
    private void callNotificationClass(String notificationDatas)
    {
        if (notificationDatas!=null && !notificationDatas.isEmpty())
        {
            System.out.println(TAG+" "+"bundle="+notificationDatas);
            Intent intent = new Intent(mActivity,NotificationActivity.class);
            intent.putExtra("notificationDatas",notificationDatas);
            intent.putExtra("isFromNotification",true);
            startActivity(intent);
            finish();
        }
    }


    private void moveToSelectLanguageScreen() {
        int TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(mActivity,LanguageSelectActivity.class));
                finish();
            }
        },TIME_OUT);
    }

    /**
     * <h>SetTimerForScreen</h>
     * <p>
     *     In this method we used to sleep screen for three second.
     * </p>
     */
    private void setTimerForScreen()
    {

        int TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(mActivity,HomePageActivity.class));
                finish();
            }
        },TIME_OUT);
    }

   private void getDefaultCurrency()
   {

       try {
           String countryIsoCode = Locale.getDefault().getCountry();
          // String countryIsoCode = Locale.getDefault().toString();
           mSessionManager.setCountryIso(countryIsoCode);
            System.out.println(TAG+" "+"locale iso cod="+countryIsoCode);
            Locale locale = new Locale("EN",countryIsoCode);
            Currency currency = Currency.getInstance(locale);
            mSessionManager.setCurrency(String.valueOf(currency));
            System.out.println(TAG+" "+"currency="+String.valueOf(currency));
        }

        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void selectLanguage(String code){
        Locale myLocale = new Locale(code);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Facebook", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Facebook", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("Facebook", "printHashKey()", e);
        }
    }




}
