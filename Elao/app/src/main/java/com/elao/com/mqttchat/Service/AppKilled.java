package com.elao.com.mqttchat.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.elao.com.mqttchat.AppController;
/**
 *
 * @since  21/06/17.
 * @version 1.0.
 * @author 3Embed.
 */
public class AppKilled extends Service
{
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("exe","destroy");


    }
    public void onTaskRemoved(Intent rootIntent)
    {
        Log.d("exe","onTaskRemoved");

        AppController.getInstance().disconnect();
        AppController.getInstance().setApplicationKilled(true);
        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId());
        stopSelf();
    }
}