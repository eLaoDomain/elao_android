package com.elao.com.utility;

import com.suresh.innapp_purches.Inn_App_billing.SkuDetails;

import java.util.List;

/**
 * Created by embed on 21/12/17.
 */
public interface SukoCallBack
{
    void  onSucess(List<SkuDetails> data);
    void onError(String message);
}
